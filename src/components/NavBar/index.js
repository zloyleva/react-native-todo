import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import {THEME} from "../../theme";
import Black from "../../containers/Text/Roboto/Black";

const NavBar = () => (
  <View style={styles.container}>
    <View style={styles.logoWrapper}>
      <Image
        style={styles.image}
        source={require('../../../assets/react.png')}
      />
    </View>
    <View style={styles.block}>
      <Black style={styles.navbar}>ToDo App. v0.1</Black>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    marginTop: 26,
    alignItems: 'stretch',
  },
  block: {
    height: 80,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: THEME.MAIN_COLOR,
    marginHorizontal: 10,
    marginBottom: 6,
    borderRadius: 5,
  },
  navbar: {
    fontSize: 40,
    color: "#fff",
  },
  logoWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 100
  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain'
  }
});

export default NavBar;
