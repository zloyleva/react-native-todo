import React from "react";
import {StyleSheet, Text, View, TouchableOpacity} from "react-native";

import Card from '../../containers/Card';

const TodoItem = ({item, onRemove, openTodo}) => (
  <TouchableOpacity
    onPress={() => openTodo(item.id)}
    onLongPress={() => onRemove(item.id)}
  >
    <Card style={styles.container}>
      <Text>{item.title}</Text>
    </Card>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
    marginVertical: 8,
    borderWidth: 1,
    borderColor: '#ddd',
    borderRadius: 2,
    padding: 10,
  },
});

export default TodoItem;
