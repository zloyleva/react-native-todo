import React, { useState } from "react";
import { StyleSheet, Text, View, TextInput, Button, Alert } from "react-native";

const AddTodo = ({ onAddToodo }) => {
  const [todo, setState] = useState("");
  const addTodoHandler = () => {
      if(todo.trim()){
        onAddToodo(todo);
        setState('');
      }else{
        Alert.alert('Pls, enter todo title');
      }
  }
  return (
    <View style={styles.container}>
      <View style={styles.block}>
        <TextInput
            style={styles.input}
            onChangeText={setState}
            value={todo}
            placeholder='Enter ToDo'
        />
        <Button title="Add todo" onPress={addTodoHandler} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // backgroundColor: '#f00',
  },
  block: {
    marginHorizontal: 10,
    marginVertical: 6
  },
  input: {
    fontSize: 20,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#aaa",
    borderRadius: 4,
    marginBottom: 4,
    padding: 4
  }
});

export default AddTodo;
