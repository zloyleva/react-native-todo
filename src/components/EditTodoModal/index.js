import React, {useState} from 'react';
import {View, Text, Modal, StyleSheet, Button, TextInput} from 'react-native';
import {THEME} from "../../theme";

const EditTodoModal = ({isShow, hideModal, value, updateTodoHandler}) => {
  const [title, setTitle] = useState(value);
  return (
    <Modal
      visible={isShow}
      animationType="slide"
    >
      <View style={styles.default}>
        <Text>Edit ToDo</Text>
        <TextInput
          value={title}
          style={styles.input}
          onChangeText={setTitle}
        />
        <View style={styles.buttons}>
          <Button
            title='Cancel'
            onPress={hideModal}
            color={THEME.DEFAULT_COLOR}
          />
          <Button title='Update' onPress={()=>updateTodoHandler(title)}/>
        </View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  default: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    marginHorizontal: 10,
  },
  buttons: {
    width: '100%',
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  input: {
    width: '100%',
    fontSize: 20,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#aaa",
    borderRadius: 4,
    marginBottom: 4,
    padding: 4
  }
});

export default EditTodoModal;
