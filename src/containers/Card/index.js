import React from 'react';
import { View, StyleSheet } from 'react-native';

const Card = props => (
  <View style={{...styles.default, ...props.style}}>
    {props.children}
  </View>
);

const styles = StyleSheet.create({
  default: {
    padding: 20,
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 8,
  }
});

export default Card;
