import React from 'react';
import { Text, StyleSheet } from 'react-native';

const Black = props => <Text style={{...styles.default, ...props.style}}>{ props.children }</Text>;

const styles = StyleSheet.create({
  default: {
    fontFamily: 'Roboto-Black'
  }
});

export default Black;