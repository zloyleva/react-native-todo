import  React, {useState} from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';
import {THEME} from "../../theme";

import EditTodoModal from '../../components/EditTodoModal';

const TodoScreen = ({todoItem,backHandler, removeTodos, updateTodo}) => {
  const [isShow, setShowModal] = useState(false);
  return (
    <View>
      <EditTodoModal
        isShow={isShow}
        hideModal={()=>setShowModal(false)}
        value={todoItem.title}
        updateTodoHandler={(title)=>updateTodo(title, todoItem.id)}
      />
      <View style={styles.container}>
        <View style={styles.button}>
          <Button title={'Back'} onPress={backHandler} color={THEME.DEFAULT_COLOR}/>
        </View>
        <Text>{todoItem.title}</Text>
        <View style={styles.buttons}>
          <View style={styles.button}>
            <Button title={'Edit'} onPress={() => setShowModal(true)} color={THEME.MAIN_COLOR}/>
          </View>
          <View style={styles.button}>
            <Button
              title={'Remove'}
              onPress={() => removeTodos(todoItem.id)}
              color={THEME.DANGER_COLOR}
            />
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
    marginVertical: 6
  },
  buttons: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  button: {
    width: '45%',
    marginVertical: 10,
  }
});

export default TodoScreen;
