import  React from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import AddTodo from "../../components/AddTodo";
import TodoItem from "../../components/TodoItem";

const MainScreen = ({ todos, AddTodoHandler, removeTodos, openTodo  }) => (
    <View style={styles.default}>
      <AddTodo onAddToodo={AddTodoHandler} />
      <FlatList
        data={todos}
        renderItem={({item}) => (
          <TodoItem
            key={String(item.id)}
            item={item}
            onRemove={removeTodos}
            openTodo={openTodo}
          />
        )}
      />
    </View>
);

const styles = StyleSheet.create({
  default: {
    flex: 1,
    backgroundColor: '#fff'
  }
});

export default MainScreen;
