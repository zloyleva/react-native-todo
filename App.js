import React, { useState } from 'react';
import { StyleSheet, View, Alert } from 'react-native';
import * as Font from 'expo-font';

import NavBar from './src/components/NavBar'
import MainScreen from "./src/screens/MainScreen";
import TodoScreen from "./src/screens/TodoScreen";
import {AppLoading} from "expo";

async function loadApplication() {
  console.log('loadApplication:')
  return await Font.loadAsync({
    'Roboto-Light': require('./assets/fonts/Roboto/Roboto-Light.ttf'),
    'Roboto-Regular': require('./assets/fonts/Roboto/Roboto-Regular.ttf'),
    'Roboto-Medium': require('./assets/fonts/Roboto/Roboto-Medium.ttf'),
    'Roboto-Black': require('./assets/fonts/Roboto/Roboto-Black.ttf'),
  });
}

export default function App() {
  const [isReady, setReadyStatus] = useState(false);
  const [selectedTodo, setSelectedTodo] = useState(null);
  const [state, setState] = useState([
    {id:1, title:'task #1'},
    {id:2, title:'task #2'},
    {id:3, title:'task #3'},
    {id:4, title:'task #4'},
    {id:5, title:'task #5'},
    {id:6, title:'task #6'},
    {id:7, title:'task #7'},
  ]);

  if(!isReady){
    return (
      <AppLoading
        startAsync={loadApplication}
        onError={err => console.log(err)}
        onFinish={() => setReadyStatus(true)}
      />
    )
  }

  const openTodo = id => setSelectedTodo(state.find(el => el.id===id).id);

  const backToMainScreen = () => setSelectedTodo(null);

  const AddTodoHandler = title => {
    setState(prev => ([
      ...prev,
      {id: new Date().toString(), title}
    ]))
  };

  const updateTodo = (title, id) => {
    setSelectedTodo(null);
    setState(state.map(el => el.id === id?{id, title}:el));
  };

  const removeTodos = id => {
    const todo = state.find(el => el.id===id);
    Alert.alert(
      'Remove todo',
      `Do you want to remove "${todo.title}"`,
      [
        {
          text: 'Cancel',
          style: 'cancel',
          onPress: ()=>{},
        },
        {
          text: 'Remove',
          style: '',
          onPress: () => {
            setSelectedTodo(null);
            setState(state.filter(item => item.id !== id));
          }
        }
      ],
      {cancelable: true},
    );
  };

  return (
    <View style={styles.default}>
      <NavBar />
      {
        selectedTodo
          ?
            <TodoScreen
              todoItem={state.find(el => el.id === selectedTodo)}
              backHandler={backToMainScreen}
              removeTodos={removeTodos}
              updateTodo={updateTodo}
            />
          :
            <MainScreen
              todos={state}
              AddTodoHandler={AddTodoHandler}
              removeTodos={removeTodos}
              openTodo={openTodo}
            />
      }
    </View>
  );
}

const styles = StyleSheet.create({
  default: {
    flex: 1,
    backgroundColor: '#fff'
  }
});
